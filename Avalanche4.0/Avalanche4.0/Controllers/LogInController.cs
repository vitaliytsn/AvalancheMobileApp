﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;
using WebApplication2.Services;

namespace WebApplication2.Controllers
{
    public class LogInController : Controller
    {
        UserService us = new UserService();
        // GET: LogIn
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Authorize(User user)
        {
            if (us.LogIn(user.UserId, user.password))
            {
                Session["UserId"] = user.UserId;
                if (us.GetUser((int)Session["UserId"]).permisions == Permisions.EndUser
                    || us.GetUser((int)Session["UserId"]).permisions == Permisions.Supplier
                    || us.GetUser((int)Session["UserId"]).permisions == Permisions.ViewOnly)
                {
                   
                    return RedirectToAction("Index", "Transaction");
                }
                if (us.GetUser((int)Session["UserId"]).permisions == Permisions.Admin)
                {
                   
                    return RedirectToAction("Index", "User");
                }
                else return View("Index");
            }
            else
            return View("Index");
        }

        // GET: LogIn/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LogIn/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LogIn/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LogIn/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LogIn/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LogIn/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LogIn/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
