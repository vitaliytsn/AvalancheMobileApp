﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Context;
using WebApplication2.Models;
using WebApplication2.Services;

namespace WebApplication2.Controllers
{
    public class UserController : Controller
    {
        UserService us = new UserService();
        // GET: Transactions
        public ActionResult Index()
        {
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                return View(tsc.users.ToList());
            }
        }

        // GET: Transactions/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Transactions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Transactions/Create
        [HttpPost]
        public ActionResult Create(User user)
        {
            try
            {
                us.AddNewUser(user);
                
                      
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                ModelState.AddModelError("UserCreateError", e.Message.ToString());//poczytaj o tym

                return View();
            }
        }

        // GET: Transactions/Edit/5
        public ActionResult Edit(int id)
        {
            return View(us.GetUser(id));
        }

        // POST: Transactions/Edit/5
        [HttpPost]
        public ActionResult Edit(User user)
        {
            try
            {
                us.EditUser(user, user);
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                ModelState.AddModelError("UserEditError", e.Message.ToString());
                return View();
            }
        }

        // GET: Transactions/Delete/5
        public ActionResult Delete(int id)
        {
            return View(us.GetUser(id));
        }

        // POST: Transactions/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                us.DeleteUser(id);

                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                return View();
            }
        }
    }
}
