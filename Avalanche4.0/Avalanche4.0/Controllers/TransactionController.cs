﻿using AvalancheChain3._0.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Context;
using WebApplication2.Models;
using WebApplication2.Services;

namespace WebApplication2.Controllers
{
    public class TransactionController : Controller
    {
        TransactionService ts = new TransactionService();
        UserService us = new UserService();
        // GET: Transaction
        public ActionResult Index()
        {
            User user = us.GetUser((int)Session["UserId"]);
            ViewBag.User = "UserId: "+Convert.ToString(user.UserId)+"  Name: "+user.Name;
            ViewBag.TransactionCount = "TransactionCount: " + Convert.ToString(user.transactioncount);
            List<Transaction> transactions = ts.GetUsersTransactions((int)Session["UserId"]);
                return View(transactions);   
        }

        // GET: Transaction/Details/5
        public ActionResult Details(int id)
        {
            return View(ts.GetTransaction(id));
        }
  
        // GET: Transaction/Create
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult Read()
        {
            return View();
        }
       

        [HttpPost]
        public ActionResult Read(Transaction transaction)
        {
            try
            {
                HashingId hi = new HashingId();
                transaction.TransactionId = hi.rehash(transaction.Hash);
                if (ts.GetTransaction(transaction.TransactionId) != null)
                {
                    Session["TransactionId"] = transaction.TransactionId;
                    return RedirectToAction("ReadedTransaction", new { id = transaction.TransactionId });

                }
                else
                {
                    ModelState.AddModelError("ReadError", "Transaction with this hash does not exist");//poczytaj o tym
                    return View();
                }
            }
            catch(Exception e)
            {
                return View();
            }
        }

        public ActionResult ReadedTransaction(int id)
        {
            return View(ts.GetTransaction(id));
        }
     
        [HttpPost]
        public ActionResult ReadedTransaction(Transaction transaction)
        {
            try
            {
                HashingId hi = new HashingId();
                transaction.TransactionId = hi.rehash(transaction.Hash);
                User user = us.GetUser((int)Session["UserId"]);
                Transaction trans = ts.GetTransaction(transaction.TransactionId);
                ts.UseTransaction(trans, user);
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                ModelState.AddModelError("PermisionsError", e.Message);
                return View();
            }
        }

        // POST: Transaction/Create
        [HttpPost]
        public ActionResult Create(Transaction transaction)
        {
            try
            {
                int num = (int)Session["UserId"];
                transaction.ProducerId = num;
                ts.AddNewTransaction(transaction);
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                return View();
            }
        }

        // GET: Transaction/Edit/5
        public ActionResult Edit(int id)
        {
            Transaction transaction = ts.GetTransaction(id);
            return View(transaction);
        }

        // POST: Transaction/Edit/5
        [HttpPost]
        public ActionResult Edit(Transaction transaction)
        {
            try
            {
                User user = us.GetUser((int)Session["UserId"]);
                ts.EditTransaction(transaction, transaction,user);
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                ModelState.AddModelError("StatusError", e.Message);//poczytaj o tym

                return View();
            }
        }

        // GET: Transaction/Delete/5
        public ActionResult Delete(int id)
        {
            return View(ts.GetTransaction(id));
        }

        // POST: Transaction/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                ts.DeleteTransaction(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
