﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models
{
    public class Transaction
    {
        public int TransactionId { get; set; }
        public TransactionStatus status { get; set; }
        public string name { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime prodactionDate { get; set; } = DateTime.Now;

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime shelfLife { get; set; }

        public string info { get; set; }
        public int ProducerId { get; set; }
        public int CurrentOwnerId { get; set; }
        
        public string Hash { get; set; }

        public virtual ICollection<Transaction> transactions { get; set; } = new HashSet<Transaction>();
       
    }
}