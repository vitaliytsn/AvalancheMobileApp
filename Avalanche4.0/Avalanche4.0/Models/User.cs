﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace WebApplication2.Models
{
    public class User
    {
        [Required(ErrorMessage = "This field is Required")]
        public int UserId { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; } = new HashSet<Transaction>();
        public Permisions permisions { get; set; }
        [Required(ErrorMessage = "This field is Required")]
        public string password { get; set; }

        public int transactioncount { get; set; }
     
    }
}