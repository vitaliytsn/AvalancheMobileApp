﻿using AvalancheChain3._0.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using WebApplication2.Context;
using WebApplication2.Models;

namespace WebApplication2.Services
{
    public class TransactionService
    {
        public void AddNewTransaction(Transaction transaction)
        {
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                UserService us = new UserService();
                User user = us.GetUser(transaction.ProducerId);
                transaction.CurrentOwnerId = transaction.ProducerId;

                if (user.transactioncount > 0)
                {
                    if(user.permisions==Permisions.Supplier)
                        if(transaction.status!=TransactionStatus.AssignedEmpty 
                            && transaction.status!=TransactionStatus.Produced)
                            throw new Exception("you cant change status on this");
                    user.transactioncount -= 1;
                    tsc.transactions.Add(transaction);
                    us.EditUser(user, user);
                    tsc.SaveChanges();

                }
                else
                    throw new Exception("you dont have any transactions");
            }
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                HashingId hi = new HashingId();
                List<Transaction> ttt= tsc.transactions.ToList();
                Transaction trans = ttt.Last();
                Transaction tans = tsc.transactions.Find(trans.TransactionId);
                trans.Hash = hi.hashid(trans.TransactionId);
                tsc.SaveChanges();
            }
        }




        public List<Transaction> GetUsersTransactions(int UserId)
        {
            using (var tsc = new TransactionSystemContext())
            {
                List<Transaction> transactions = new List<Transaction>();
                List<Transaction> test = tsc.transactions.ToList();

                foreach (Transaction t in test)
                {
                    if (t.CurrentOwnerId == UserId)
                    {
                        transactions.Add(t);
                    }
                }
                return transactions;
            }
        }
        public void EditTransaction(Transaction oldTransaction, Transaction newTransaction,User currentUser)
        {
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                Transaction us = tsc.transactions.Find(oldTransaction.TransactionId);
                us.info = newTransaction.info;
                us.name = newTransaction.name;
                us.shelfLife = newTransaction.shelfLife;
                us.prodactionDate = newTransaction.prodactionDate;
                us.status = newTransaction.status;
               
                //  us.ProducerId = newTransaction.ProducerId;
                // us.CurrentOwnerId = newTransaction.CurrentOwnerId;
                   if (currentUser.permisions == Permisions.Supplier)
                       if (us.status != TransactionStatus.AssignedEmpty
                           && us.status != TransactionStatus.Produced)
                           throw new Exception("you cant change status on this");
                tsc.SaveChanges();
            }
        }
        public Transaction GetTransaction(int TransactionId)
        {
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                return tsc.transactions.Find(TransactionId);
            }
        }
        //TODO ADD Transaction owner
        public void UseTransaction(Transaction transaction, User user)
        {
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                if (transaction.status != TransactionStatus.Used)
                {
                    if (user.permisions != Permisions.Admin
                    && user.permisions != Permisions.ViewOnly
                    && user.permisions != Permisions.Supplier)
                    {
                        transaction.CurrentOwnerId = user.UserId;
                        transaction.status = TransactionStatus.Used;
                        Transaction us = tsc.transactions.Find(transaction.TransactionId);
                        us.status = transaction.status;
                        us.CurrentOwnerId = transaction.CurrentOwnerId;
                        tsc.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("you cant use transaction check your permissions");
                    }
                }
                else
                {
                    throw new Exception("That Transaction is already Used");
                }
            }
        }

        public void DeleteTransaction(int transactionid)
        {
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                Transaction transaction = tsc.transactions.Find(transactionid);
                tsc.transactions.Remove(transaction);
                tsc.SaveChanges();
            }
        }

    }
}