﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.Context;
using WebApplication2.Models;

namespace WebApplication2.Services
{
    public class UserService
    {

        public void AddNewUser(User user)
        {
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                
                if (user.permisions != Permisions.Supplier)
                    if (user.transactioncount > 0)
                        throw new Exception("This user cant have any transactions");

                tsc.users.Add(user);
                tsc.SaveChanges();
            }
        }
        public void EditUser(User oldUser, User newUser)
        {
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                User us = tsc.users.Find(oldUser.UserId);
                us.Name = newUser.Name;
                us.SurName = newUser.SurName;
                us.permisions = newUser.permisions;
                us.password = newUser.password;
                us.transactioncount = newUser.transactioncount;
                if (us.permisions != Permisions.Supplier)
                    if(us.transactioncount > 0)
                        throw new Exception("This user cant have any transactions");
                tsc.SaveChanges();
            }
        }
        public User GetUser(int UserId)
        {
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                return tsc.users.Find(UserId);
            }
        }
        public void DeleteUser(int UserId)
        {
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                User user = tsc.users.Find(UserId);
                tsc.users.Remove(user);
                tsc.SaveChanges();
            }
        }
        public bool LogIn(int UserId, string password)
        {
            using (TransactionSystemContext tsc = new TransactionSystemContext())
            {
                User us = tsc.users.Find(UserId);
                if (us.password == password) return true;
                else throw new Exception("access denied");
            }
        }
    }
}