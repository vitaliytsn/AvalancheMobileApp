﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication2.Models;

namespace WebApplication2.Context
{
    public class TransactionSystemContext:DbContext
    {
        public DbSet<User> users { get; set; }
        public DbSet<Transaction> transactions { get; set; }

        protected  override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
            .HasMany(a => a.Transactions);
        }

    }
}